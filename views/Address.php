<?php 

require_once("models/AddressModel.php");
require_once("viewmodels/AddressViewModel.php");

$model = new AddressModel();
$viewModel = new AddressViewModel($model);

?>

<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">


<div class="container" style="margin-top:50px;">
    
	<div class="row justify-content-center">
		<div class="col-12 col-md-8 col-lg-6 pb-5">

				<div class="card border-primary rounded-0" style="border:none">
					<div class="card-header p-0">
						<div class="bg-info text-white text-center py-2" style="background-color:#06a0e3 !important;">
							<h3>Wunder Fleet User Registration</h3>
						</div>
					</div>
				</div>
                   
        </div>
	</div>
	
	
	<div class="row justify-content-center">
		<div class="col-12 col-md-8 col-lg-6 pb-5">


                    <form  id="address_info" method="post" >
                        <div class="card border-primary rounded-0">
                            <div class="card-header p-0">
                                <div class="bg-info text-white text-center py-2" style="background-color:#06a0e3 !important;">
                                    <h3><i class="fa fa-envelope"></i>Step 2. Address Info</h3>
                                  
                                </div>
                            </div>
                            <div class="card-body p-3">

                                <div class="form-group">
                                    <div class="input-group mb-2">
                                        <input type="text" class="form-control" id="street" name="street" value="<?php if(isset($_COOKIE["custTempCode"])) echo $viewModel->getAddress($_COOKIE["custTempCode"])["street"] ?>" placeholder="Street" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group mb-2">
                                        <input type="number" class="form-control" id="houseNumber" name="houseNumber" value="<?php if(isset($_COOKIE["custTempCode"])) echo $viewModel->getAddress($_COOKIE["custTempCode"])["houseNumber"] ?>" placeholder="House Number" required>
                                    </div>
                                </div>
								<div class="form-group">
                                    <div class="input-group mb-2">
                                        <input type="number" class="form-control" id="zipCode" name="zipCode" value="<?php if(isset($_COOKIE["custTempCode"])) echo $viewModel->getAddress($_COOKIE["custTempCode"])["zipCode"] ?>" placeholder="Zip Code" required>
                                    </div>
                                </div>
								<div class="form-group">
                                    <div class="input-group mb-2">
                                        <input type="text" class="form-control" id="city" name="city" value="<?php if(isset($_COOKIE["custTempCode"])) echo $viewModel->getAddress($_COOKIE["custTempCode"])["city"] ?>" placeholder="City" required>
                                    </div>
                                </div>
								
								<input type = "hidden" name="customerTempCode" value="<?php if(isset($_COOKIE["custTempCode"]))  echo $_COOKIE["custTempCode"] ?>" >
                                
								<div class="text-center">
                                    <input type="button" onclick="window.location = '/wunderfleet/personal/noredirect'" value="<<Previous" class="btn btn-info rounded-0 py-2" style="background-color:#06a0e3 !important;">
									<input type="submit" value="Next>>" class="btn btn-info  rounded-0 py-2" style="background-color:#06a0e3 !important;">
                                </div>
                            </div>

                        </div>
                    </form>
                   

                </div>
	</div>
</div>


<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<script>
		$(document).ready(function(){

			setCookie("step2","step2",1);
			
			var currentUrl = window.location.href;
			var splitted = currentUrl.split("/");
			
			if(splitted[5]!="noredirect")
			{
				
				if(getCookie("step3") == "step3")
					window.location = "/wunderfleet/payment";
			
			}
			
			$("#address_info").submit(function(event){
				event.preventDefault();
				
				$.ajax({
					method: "POST",
					url: "/wunderfleet/saveAddress",
					data : $('form').serialize() ,
					success: function(data){
						
						console.log(data);
						window.location = "/wunderfleet/payment/noredirect";
						
					},
					error: function(xhr, desc, err){
						console.log(err);
						
					}
				});
			});
			
		});
		
		function setCookie(cname, cvalue, exdays) {
		  var d = new Date();
		  d.setTime(d.getTime() + (exdays*24*60*60*1000));
		  var expires = "expires="+ d.toUTCString();
		  document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
		}

		function getCookie(cname) {
		  var name = cname + "=";
		  var decodedCookie = decodeURIComponent(document.cookie);
		  var ca = decodedCookie.split(';');
		  for(var i = 0; i <ca.length; i++) {
			var c = ca[i];
			while (c.charAt(0) == ' ') {
			  c = c.substring(1);
			}
			if (c.indexOf(name) == 0) {
			  return c.substring(name.length, c.length);
			}
		  }
		  return "";
		}
		
</script>