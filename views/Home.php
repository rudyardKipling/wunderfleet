
<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">

<div class="container" style="margin-top:50px;">

	 <div class="row justify-content-center">
		<div class="col-12 col-md-8 col-lg-6 pb-5">

				<div class="card border-primary rounded-0" style="border:none">
					<div class="card-header p-0">
						<div class="bg-info text-white text-center py-2" style="background-color:#06a0e3 !important;">
							<h3>Wunder Fleet User Registration</h3>
						</div>
					</div>
				</div>
                   
        </div>
	</div>
    
	<div class="row justify-content-center">
		<div class="col-12 col-md-8 col-lg-6 pb-5">


                    <form id="personal_info" method="post">
                        <div class="card border-primary rounded-0">
                            <div class="card-header p-0">
                                
                            </div>
                            <div class="card-body p-3">

                                <div class="text-center">
								
									<input type="button" onclick="window.location = '/wunderfleet/personal'" value="Register Now" class="btn btn-info  rounded-0 py-2" style="background-color:#06a0e3 !important;">
									
								</div>
                            </div>

                        </div>
                    </form>
                </div>
	</div>
</div>

<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
