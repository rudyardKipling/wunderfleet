<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">

<div class="container" style="margin-top:50px;">

	 <div class="row justify-content-center">
		<div class="col-12 col-md-8 col-lg-6 pb-5">

				<div class="card border-primary rounded-0" style="border:none">
					<div class="card-header p-0">
						<div class="bg-info text-white text-center py-2" style="background-color:#06a0e3 !important;">
							<h3>Wunder Fleet User Registration</h3>
						</div>
					</div>
				</div>
                   
        </div>
	</div>
    
	<div class="row justify-content-center">
		<div class="col-12 col-md-8 col-lg-6 pb-5">


                    <form id="personal_info" method="post">
                        <div class="card border-primary rounded-0">
                            <div class="card-header p-0">
                                <div class="bg-info text-white text-center py-2" style="background-color:#06a0e3 !important;">
                                    <h3><i class="fa fa-envelope"></i>You have registered successfully</h3>
                                  
                                </div>
                            </div>
                            <div class="card-body p-3">

                                <div class="text-center">
								
									<b>Your paymentDataId :</b>  <span id="paymentDataId" ></span>
									
								</div>
                            </div>

                        </div>
                    </form>
                </div>
	</div>
</div>


<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<script>
		$(document).ready(function(){

			$("#paymentDataId").text(getCookie("paymentDataId"));
			
		});
		
		function setCookie(cname, cvalue, exdays) {
		  var d = new Date();
		  d.setTime(d.getTime() + (exdays*24*60*60*1000));
		  var expires = "expires="+ d.toUTCString();
		  document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
		}

		function getCookie(cname) {
		  var name = cname + "=";
		  var decodedCookie = decodeURIComponent(document.cookie);
		  var ca = decodedCookie.split(';');
		  for(var i = 0; i <ca.length; i++) {
			var c = ca[i];
			while (c.charAt(0) == ' ') {
			  c = c.substring(1);
			}
			if (c.indexOf(name) == 0) {
			  return c.substring(name.length, c.length);
			}
		  }
		  return "";
		}
		
</script>