<?php 

require_once("models/PersonalModel.php");
require_once("viewmodels/PersonalViewModel.php");

$model = new PersonalModel();
$viewModel = new PersonalViewModel($model);


$cookie_value = time().mt_rand();
if(!isset($_COOKIE["custTempCode"])) {
	
	setcookie("custTempCode",  $cookie_value, time() + (86400 * 30), "/");

}

?>


<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">


<div class="container" style="margin-top:50px;">

	 <div class="row justify-content-center">
		<div class="col-12 col-md-8 col-lg-6 pb-5">

				<div class="card border-primary rounded-0" style="border:none">
					<div class="card-header p-0">
						<div class="bg-info text-white text-center py-2" style="background-color:#06a0e3 !important;">
							<h3>Wunder Fleet User Registration</h3>
						</div>
					</div>
				</div>
                   
        </div>
	</div>
    
	<div class="row justify-content-center">
		<div class="col-12 col-md-8 col-lg-6 pb-5">


                    <form id="personal_info" method="post">
                        <div class="card border-primary rounded-0">
                            <div class="card-header p-0">
                                <div class="bg-info text-white text-center py-2" style="background-color:#06a0e3 !important;">
                                    <h3><i class="fa fa-envelope"></i>Step 1. Personal Info</h3>
                                  
                                </div>
                            </div>
                            <div class="card-body p-3">

                                <div class="form-group">
                                    <div class="input-group mb-2">
                                        <input type="text" class="form-control" id="firstname" name="firstname" value="<?php if(isset($_COOKIE["custTempCode"])) echo $viewModel->getPersonal($_COOKIE["custTempCode"])["firstname"] ?>" placeholder="First Name" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group mb-2">
                                        <input type="text" class="form-control" id="lastname" name="lastname" value="<?php if(isset($_COOKIE["custTempCode"])) echo $viewModel->getPersonal($_COOKIE["custTempCode"])["lastname"] ?>" placeholder="Last Name" required>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="input-group mb-2">
                                        <input type="text" class="form-control" id="telephone" name="telephone" value="<?php if(isset($_COOKIE["custTempCode"])) echo $viewModel->getPersonal($_COOKIE["custTempCode"])["telephone"] ?>" placeholder="Telephone" required>
                                    </div>
                                </div>
										<input type = "hidden" name="customerTempCode" value="<?php if(!isset($_COOKIE["custTempCode"]))  echo $cookie_value; else echo $_COOKIE["custTempCode"] ?>" >
										
                                <div class="text-center">
                                    <input type="submit" value="Next>>" class="btn btn-info  rounded-0 py-2" style="background-color:#06a0e3 !important;">
                                </div>
                            </div>

                        </div>
                    </form>
                </div>
	</div>
</div>

<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<script>
		$(document).ready(function(){

			setCookie("step1","step1");
			
			var currentUrl = window.location.href;
			var splitted = currentUrl.split("/");
			
			console.log(getCookie("step2"));
			
			if(splitted[5]!="noredirect")
			{
				
				if(getCookie("step3") == "step3")
					window.location = "/wunderfleet/payment";
				
				else if(getCookie("step2") == "step2")
					window.location = "/wunderfleet/address";
			
			}
			
			
			$("#personal_info").submit(function(event){
				event.preventDefault();
				
				
				$.ajax({
					method: "POST",
					url: "/wunderfleet/savePersonal",
					data : $('form').serialize() ,
					success: function(data){
						
						console.log(data);
						window.location = "/wunderfleet/address/noredirect";
						
					},
					error: function(xhr, desc, err){
						console.log(err);
						
					}
				});
			});
			
		});
		
		
		function setCookie(cname, cvalue, exdays) {
		  var d = new Date();
		  d.setTime(d.getTime() + (exdays*24*60*60*1000));
		  var expires = "expires="+ d.toUTCString();
		  document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
		}

		function getCookie(cname) {
		  var name = cname + "=";
		  var decodedCookie = decodeURIComponent(document.cookie);
		  var ca = decodedCookie.split(';');
		  for(var i = 0; i <ca.length; i++) {
			var c = ca[i];
			while (c.charAt(0) == ' ') {
			  c = c.substring(1);
			}
			if (c.indexOf(name) == 0) {
			  return c.substring(name.length, c.length);
			}
		  }
		  return "";
		}
		
</script>
