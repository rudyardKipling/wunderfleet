<?php 

require_once("models/PaymentModel.php");
require_once("viewmodels/PaymentViewModel.php");

$model = new PaymentModel();
$viewModel = new PaymentViewModel($model);

?>

<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">

<div class="container" style="margin-top:50px;">
    
	<div class="row justify-content-center">
		<div class="col-12 col-md-8 col-lg-6 pb-5">

				<div class="card border-primary rounded-0" style="border:none">
					<div class="card-header p-0">
						<div class="bg-info text-white text-center py-2" style="background-color:#06a0e3 !important;">
							<h3>Wunder Fleet User Registration</h3>
						</div>
					</div>
				</div>
                   
        </div>
	</div>
	
	
	<div class="row justify-content-center">
		<div class="col-12 col-md-8 col-lg-6 pb-5">


                    <form id="payment_info" method="post">
                        <div class="card border-primary rounded-0">
                            <div class="card-header p-0">
                                <div class="bg-info text-white text-center py-2" style="background-color:#06a0e3 !important;">
                                    <h3><i class="fa fa-envelope"></i>Step 3. Payment Info</h3>
                                  
                                </div>
                            </div>
                            <div class="card-body p-3">

                                <div class="form-group">
                                    <div class="input-group mb-2">
                                        <input type="text" class="form-control" id="iban" name="iban" value="<?php if(isset($_COOKIE["custTempCode"])) echo $viewModel->getPayment($_COOKIE["custTempCode"])["iban"] ?>" placeholder="IBAN" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group mb-2">
                                        <input type="text" class="form-control" id="owner" name="owner" value="<?php if(isset($_COOKIE["custTempCode"])) echo $viewModel->getPayment($_COOKIE["custTempCode"])["owner"] ?>" placeholder="Owner" required>
                                    </div>
                                </div>
								
								<input type = "hidden" name="customerTempCode" value="<?php if(isset($_COOKIE["custTempCode"]))  echo $_COOKIE["custTempCode"] ?>" >

                                <div class="text-center">
                                    <input type="submit" onclick="window.location = '/wunderfleet/address/noredirect'" value="<<Previous" class="btn btn-info rounded-0 py-2" style="background-color:#06a0e3 !important;">
									<input type="submit" value="Finish" class="btn btn-info  rounded-0 py-2" style="background-color:#06a0e3 !important;">
                                </div>
                            </div>

                        </div>
                    </form>
                   

                </div>
	</div>
</div>


<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<script>
		$(document).ready(function(){

			setCookie("step3", "step3", 1);
			

			$("#payment_info").submit(function(event){
				event.preventDefault();
				
				$.ajax({
					method: "POST",
					url: "/wunderfleet/savePayment",
					data : $('form').serialize() ,
					success: function(data){
						
						
						//save all user data
						$.ajax({
							method: "POST",
							url: "/wunderfleet/saveUser",
							type:"json",
							data : $('form').serializeArray() ,
							success: function(data){
								
								var paymentDataId = data;
								setCookie("paymentDataId",paymentDataId,1);
								
								//unset page step cookies
								setCookie("step1",paymentDataId,0);
								setCookie("step2",paymentDataId,0);
								setCookie("step3",paymentDataId,0);
								
								window.location = "/wunderfleet/success";
								
							},
							error: function(xhr, desc, err){
								console.log(err);
								
							}
						});

						
					},
					error: function(xhr, desc, err){
						console.log(err);
						
					}
				});
			});
			
		});
		
		
		function setCookie(cname, cvalue, exdays) {
		  var d = new Date();
		  d.setTime(d.getTime() + (exdays*24*60*60*1000));
		  var expires = "expires="+ d.toUTCString();
		  document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
		}

		function getCookie(cname) {
		  var name = cname + "=";
		  var decodedCookie = decodeURIComponent(document.cookie);
		  var ca = decodedCookie.split(';');
		  for(var i = 0; i <ca.length; i++) {
			var c = ca[i];
			while (c.charAt(0) == ' ') {
			  c = c.substring(1);
			}
			if (c.indexOf(name) == 0) {
			  return c.substring(name.length, c.length);
			}
		  }
		  return "";
		}
		
</script>