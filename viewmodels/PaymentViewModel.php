<?php 

require_once("models/PaymentModel.php");

class PaymentViewModel {
	private $payment;	
	private $tempcode;
	
	
	public function __construct(PaymentModel $payment) {
		$this->payment = $payment;
	}
	
	public function getPayment($tempcode) {
		return $this->payment->findPaymentByTempCode($tempcode);
	}
	
	public function savePayment(){
		return $this->payment->updatePayment($_POST);
	}
	
}


?>