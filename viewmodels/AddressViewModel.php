<?php 

require_once("models/AddressModel.php");

class AddressViewModel {
	private $address;	
	private $tempcode;
	
	public function __construct(AddressModel $address) {
		$this->address = $address;
	}
	
	public function getAddress($tempcode) {
		return $this->address->findAddressByTempCode($tempcode);
	}
	
	public function saveAddress(){
		return $this->address->updateAddress($_POST);
	}
	
}


?>