<?php 

require_once("models/PersonalModel.php");

class PersonalViewModel {
	private $personal;	
	private $tempcode;
	
	
	public function __construct(PersonalModel $personal) {
		$this->personal = $personal;
	}
	
	public function getPersonal($tempcode) {
		return $this->personal->findPersonalByTempCode($tempcode);
	}
	
	public function savePersonal(){
		
		if(!$this->personal->checkDuplicateRecord($_POST["customerTempCode"]))
			return $this->personal->insertPersonal($_POST);
		else
			return $this->personal->updatePersonal($_POST);
	}
	
}


?>