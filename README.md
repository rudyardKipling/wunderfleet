**Wunder Fleet backend assignment**

This project is structured with MVVM architectural pattern. Although there are too many MVC structured frameworks are being used by community, there is not too much example of MVVM structured projects with PHP.   

I hope this project helps someone to understand basics of MVVM structure written in PHP.

---

## How to use


1. Download the files with zip format or git.
2. Extract the files if you downloaded as zip.
3. Move the files to your server.
4. Call the project from its root folder as "http://localhost/wunderfleet"
5. You are ready to go. Register as a new user with MVVM structure.

---

**Configuration**
---

Create ```.htaccess``` in root directory (or project directory) and put the following code to the file.

```
RewriteEngine On
RewriteCond %{REQUEST_FILENAME} !-f
RewriteCond %{REQUEST_FILENAME} !-d
RewriteRule ^(.*)$ index.php/$1 [NC,L,QSA]
```

Change connection parameters for your MySQL database from "database/Config.php" 
