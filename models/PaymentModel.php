<?php 


class PaymentModel {
	private $db;

    public function __construct() {
		
		$host = HOST; $db   = DATABASE; $user = USER; $pass = PASS; $charset = 'utf8';
		$dsn = "mysql:host=$host;dbname=$db;charset=$charset";
		$opt = [
			PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
			PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
			PDO::ATTR_EMULATE_PREPARES   => false,
		];
		$this->db = new PDO($dsn, $user, $pass, $opt);
    }


	
	public function findPaymentByTempCode($temp_code) {
		
		$stmt = $this->db->prepare("SELECT owner, iban FROM temp_user WHERE customerTempCode = :temp_code"); 
		$stmt->bindValue(':temp_code', $temp_code, PDO::PARAM_STR);
		$stmt->execute(); 
		return $stmt->fetch();

	}
	
	public function updatePayment(array $post_values) {
		$statement =  $this->db->prepare('UPDATE temp_user SET iban = :iban, owner = :owner WHERE customerTempCode = :customerTempCode');

		return $statement->execute([
			'iban' => $post_values["iban"],
			'owner' => $post_values["owner"],
			'customerTempCode' => $post_values["customerTempCode"]
		]);
		
	}
	
	
	
}




?>