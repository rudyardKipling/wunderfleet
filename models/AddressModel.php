<?php 

class AddressModel {
	private $db;

    public function __construct() {
		
		$host = HOST; $db   = DATABASE; $user = USER; $pass = PASS; $charset = 'utf8';
		$dsn = "mysql:host=$host;dbname=$db;charset=$charset";
		$opt = [
			PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
			PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
			PDO::ATTR_EMULATE_PREPARES   => false,
		];
		$this->db = new PDO($dsn, $user, $pass, $opt);
    }


	
	public function findAddressByTempCode($temp_code) {
		
		$stmt = $this->db->prepare("SELECT street, houseNumber, zipCode, city FROM temp_user WHERE customerTempCode = :temp_code"); 
		$stmt->bindValue(':temp_code', $temp_code, PDO::PARAM_STR);
		$stmt->execute(); 
		return $stmt->fetch();

	}
	
	public function updateAddress(array $post_values) {
		
		$statement =  $this->db->prepare('UPDATE temp_user set street = :street, houseNumber = :houseNumber, zipCode = :zipCode, city = :city where customerTempCode = :customerTempCode');

		return $statement->execute([
			'street' => $post_values["street"],
			'houseNumber' => $post_values["houseNumber"],
			'zipCode' => $post_values["zipCode"],
			'city' => $post_values["city"],
			'customerTempCode' => $post_values["customerTempCode"]
		]);
		
	}
	
	
	
}




?>