<?php 

class PersonalModel {
	private $db;

    public function __construct() {
		
		$host = HOST; $db   = DATABASE; $user = USER; $pass = PASS; $charset = 'utf8';
		$dsn = "mysql:host=$host;dbname=$db;charset=$charset";
		$opt = [
			PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
			PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
			PDO::ATTR_EMULATE_PREPARES   => false,
		];
		$this->db = new PDO($dsn, $user, $pass, $opt);
    }


	public function findPersonalByTempCode($temp_code) {
		
		$stmt = $this->db->prepare("SELECT firstname, lastname, telephone FROM temp_user WHERE customerTempCode = :temp_code"); 
		$stmt->bindValue(':temp_code', $temp_code, PDO::PARAM_STR);
		$stmt->execute(); 
		return $stmt->fetch();

	}
	
	
	public function checkDuplicateRecord($temp_code) {
		
		$stmt = $this->db->prepare("SELECT * FROM temp_user WHERE customerTempCode = :temp_code"); 
		$stmt->bindValue(':temp_code', $temp_code, PDO::PARAM_STR);
		$stmt->execute(); 
		$stmt->fetch();
		
		if($stmt->fetch())
			return true;
		else
			return false;

	}
	



	
	public function updatePersonal(array $post_values) {
		$statement =  $this->db->prepare('UPDATE temp_user SET firstname = :firstname, lastname = :lastname, telephone = :telephone WHERE customerTempCode = :customerTempCode');

		return $statement->execute([
			'firstname' => $post_values["firstname"],
			'lastname' => $post_values["lastname"],
			'telephone' => $post_values["telephone"],
			'customerTempCode' => $post_values["customerTempCode"]
		]);
		
	}
	
	public function insertPersonal(array $post_values) {
		$statement =  $this->db->prepare('INSERT INTO temp_user (customerTempCode, firstname, lastname, telephone) VALUES (:customerTempCode, :firstname, :lastname, :telephone)');

		return $statement->execute([
			'firstname' => $post_values["firstname"],
			'lastname' => $post_values["lastname"],
			'telephone' => $post_values["telephone"],
			'customerTempCode' => $post_values["customerTempCode"]
		]);
		
	}
	
	
	
}




?>