<?php 

class UserModel {
	private $db;

    public function __construct() {
		
		$host = HOST; $db   = DATABASE; $user = USER; $pass = PASS; $charset = 'utf8';
		$dsn = "mysql:host=$host;dbname=$db;charset=$charset";
		$opt = [
			PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
			PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
			PDO::ATTR_EMULATE_PREPARES   => false,
		];
		$this->db = new PDO($dsn, $user, $pass, $opt);
    }

	
	
	
	public function insertUser(array $post_values) {
		
		$statement =  $this->db->prepare('INSERT INTO user (firstname, lastname, telephone, street, houseNumber, zipCode, city, owner, iban)
SELECT firstname, lastname, telephone, street, houseNumber, zipCode, city, owner, iban FROM temp_user WHERE customerTempCode = :customerTempCode');

		$statement->execute([
			'customerTempCode' => $post_values["customerTempCode"]
		]);
		
		$lastInsertId = $this->db->lastInsertId(); //this is last inserted customerId
		
		//creta an array for encoding into json for posting WunderFleet's web service 
		$paymentArr = array('customerId' => $lastInsertId, 'iban' => $post_values["iban"], 'owner' =>  $post_values["owner"]);
		//convert to json
		$payload = json_encode($paymentArr);
		
		//make api call
		$opts = array(
		  "http" => array(
			"method" => "POST",
			"header" => "Content-Type: application/x-www-form-urlencoded",
			"content" => $payload
		  )
		);
		$context = stream_context_create($opts);
		$response = file_get_contents('https://37f32cl571.execute-api.eu-central-1.amazonaws.com/default/wunderfleet-recruiting-backend-dev-save-payment-data', false, $context);

		$responseArray = json_decode($response);
		
		$paymentDataId = $responseArray->paymentDataId;
		
		$statement =  $this->db->prepare('UPDATE user set paymentDataId = :paymentDataId WHERE customerId = :customerId');

		$statement->execute([
			'paymentDataId' => $paymentDataId,
			'customerId' => $lastInsertId
		]);
		
		
		$statement =  $this->db->prepare('DELETE from temp_user  WHERE customerTempCode = :customerTempCode');
		$statement->execute([
			'customerTempCode' => $post_values["customerTempCode"]
		]);
		
		echo $paymentDataId ;
	}
	
	
	
}




?>