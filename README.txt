1. Describe possible performance optimizations for your Code.

- A caching library can be used to decrease response time of application and decrease the load of backend.
- Double quotes "" can be replaced with single quotes ''. It is a common thinking that single qoutes are better for performance.
- require_once can be  used less.
- "===" can be used instead of "==". 

2. Which things could be done better, than you've done it?

- First i looked at what was written in the assignment, i thought that this would be a very easy coding challenge. But later on i realised that, coding with MVP/MVI/MVVM would be a big plus, then i wanted to give it a try. I decided to structure the code with MVVM. Although there are so many examples about MVVM written in C# on internet, it was very challenging to find documents, articles about MVVM structure written in PHP. Then i have read about discussions in forums about differences and similarities of architectural patterns like MVC/MVI/MVP/MVVM. This reading process evolved my look of view about architectural patterns. Then i started to think that, i can now  create a project with MVVM pattern. Although there are some missing things in the project i wrote, it would be a good example for understanding basics of MVVM.
- A router had to be implemented the structure. Then i used a third party php library.
- User interface could be more user friendly and bautiful. I used bootsrap for user interface but some other libraries could also be used like react.js, knockout.js for data manipulation.
