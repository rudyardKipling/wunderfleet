<?php

include("database/config.php");
require 'third_party/router.class.php';
require_once("models/PersonalModel.php");
require_once("models/AddressModel.php");
require_once("models/PaymentModel.php");
require_once("models/UserModel.php");
require_once("viewmodels/PersonalViewModel.php");
require_once("viewmodels/AddressViewModel.php");
require_once("viewmodels/PaymentViewModel.php");
require_once("viewmodels/UserViewModel.php");

$router = new Router;

$personalModel = new PersonalModel();
$personalViewModel = new PersonalViewModel($personalModel);

$addressModel = new AddressModel();
$addressViewModel = new AddressViewModel($addressModel);

$paymentModel = new PaymentModel();
$paymentViewModel = new PaymentViewModel($paymentModel);

$userModel = new UserModel();
$userViewModel = new UserViewModel($userModel);


$router->get('', function(){
	require "views/Home.php";
});


$router->get('/personal', function(){
	require "views/Personal.php";
});
$router->get('/personal/noredirect', function(){
	require "views/Personal.php";
});
$router->post('/savePersonal', array(
	'func' => array($personalViewModel, 'savePersonal')
));



$router->get('/address', function(){
	require "views/Address.php";
});
$router->get('/address/noredirect', function(){
	require "views/Address.php";
});
$router->post('/saveAddress', array(
	'func' => array($addressViewModel, 'saveAddress')
));



$router->get('/payment', function(){
	require "views/Payment.php";
});
$router->get('/payment/noredirect', function(){
	require "views/Payment.php";
});
$router->post('/savePayment', array(
	'func' => array($paymentViewModel, 'savePayment')
));


$router->post('/saveUser', array(
	'func' => array($userViewModel, 'saveUser')
));
$router->get('/success', function(){
	require "views/User.php";
});


$router->catch_exception(function(){
	echo 'no suitable routing pattern';
});

$router->match();